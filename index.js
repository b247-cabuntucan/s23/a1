/*
Activity Instruction:

1. In the s23 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
Name (String)
Age (Number)
Pokemon (Array)
Friends (Object with Array values for properties)
// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// 6. Access the trainer object properties using dot and square bracket notation.
// 7. Invoke/call the trainer talk object method.
// 8. Create a constructor for creating a pokemon with the following properties:
// Name (Provided as an argument to the contructor)
// Level (Provided as an argument to the contructor)
// Health (Create an equation that uses the level property)
// Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.

*/

//Activity 1
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charlizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoen: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);
console.log("Trainer's Name: " + trainer.name);
console.log("Trainer's Age: " + trainer.age);
console.log("Trainer's Pokemons: " + trainer["pokemon"]);
console.log("Trainer's Friends from Hoen: " + trainer["friends"]["hoen"]);
console.log("Trainer's Friends from Kanto: " + trainer["friends"]["kanto"]);
trainer.talk();

//Activity 2

function pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function(target){
        let healthLeft = target.health - this.attack
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + " health is now reduced to " + (healthLeft));
        
       if (healthLeft <= 0) {
        target.faint();
       }

        return target.health = healthLeft;
        
        
    };
    

    this.faint = function(){
        console.log(this.name + ' fainted. ');
    };

   
}


let pikachu = new pokemon("Pikachu", 12);
let geodude = new pokemon("Geodude", 8);
let mewtwo = new pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);

